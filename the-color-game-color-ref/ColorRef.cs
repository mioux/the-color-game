﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: SRUMEU
 * Date: 15/04/2015
 * Heure: 16:35
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using System.Collections.Generic;
using System.Drawing;

namespace the_color_game_color_ref
{
	/// <summary>
	/// Description of MyClass.
	/// </summary>
	public class ColorRef
	{
		/// <summary>
		/// Valeur de la couleur.
		/// </summary>
		public Color ColorValue { get; private set; }
		
		/// <summary>
		/// Nom de la couleur.
		/// </summary>
		public string ColorName { get; private set; }
		
		/// <summary>
		/// Constructeur.
		/// </summary>
		private ColorRef(byte idx, bool isHard)
		{
			if (idx < 8)
			{
				ColorValue = isHard ? _hardColors[idx] : _easyColors[idx];
				ColorName = isHard ? _hardNames[idx] : _easyNames[idx];
			}
			else
			{
				throw new IndexOutOfRangeException("L'index doit être compris entre 0 et 7");
			}
		}
		
		/// <summary>
		/// Liste des couluers "mode facile".
		/// </summary>
		private static Color[] _easyColors = new Color[]
                                                 { Color.Red,
                                                   Color.Green,
                                                   Color.Blue,
                                                   Color.Cyan,
                                                   Color.Magenta,
                                                   Color.Yellow,
                                                   Color.Black,
                                                   Color.White };
		
		/// <summary>
		/// Liste des couleurs "mode extrême".
		/// </summary>
		private static Color[] _hardColors = new Color[]
                                                 { Color.LightCyan,
												   Color.DodgerBlue,
												   Color.Cyan,
												   Color.DeepSkyBlue,
												   Color.Azure,
												   Color.Blue,
												   Color.LightSkyBlue,
												   Color.SkyBlue
                                                 };
		
		/// <summary>
		/// Nom des couleurs "mode facile".
		/// </summary>
		private static string[] _easyNames = new string[]
												 { "Rouge",
												   "Vert",
												   "Bleu",
												   "Cyan",
												   "Magenta",
												   "Jaune",
												   "Noir",
												   "Blanc"
												 };
		
		/// <summary>
		/// Nom des couleurs "mode extrême".
		/// </summary>
		private static string[] _hardNames = new string[]
												 { "LightCyan",
												   "DodgerBlue",
												   "Cyan",
												   "DeepSkyBlue",
												   "Azure",
												   "Blue",
												   "LightSkyBlue",
												   "SkyBlue"
												 };
		
		/// <summary>
		/// Récupération des paramètres d'une couleur.
		/// </summary>
		/// <param name="idx">Index de la couleur.</param>
		/// <param name="isHard">Mode "extree</param>
		/// <returns></returns>
		
		public static ColorRef FromIndex(byte idx, bool isHard)
		{
			return new ColorRef(idx, isHard);
		}
	}
}