﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: SRUMEU
 * Date: 16/04/2015
 * Heure: 10:11
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
namespace the_color_game_winform_version
{
	partial class frm_memory
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.txt_remaining = new System.Windows.Forms.TextBox();
			this.cardPanel1 = new the_color_game_winform_version.CardPanel();
			this.cardPanel2 = new the_color_game_winform_version.CardPanel();
			this.cardPanel3 = new the_color_game_winform_version.CardPanel();
			this.cardPanel4 = new the_color_game_winform_version.CardPanel();
			this.cardPanel5 = new the_color_game_winform_version.CardPanel();
			this.cardPanel6 = new the_color_game_winform_version.CardPanel();
			this.cardPanel7 = new the_color_game_winform_version.CardPanel();
			this.cardPanel8 = new the_color_game_winform_version.CardPanel();
			this.cardPanel9 = new the_color_game_winform_version.CardPanel();
			this.cardPanel10 = new the_color_game_winform_version.CardPanel();
			this.cardPanel11 = new the_color_game_winform_version.CardPanel();
			this.cardPanel12 = new the_color_game_winform_version.CardPanel();
			this.cardPanel13 = new the_color_game_winform_version.CardPanel();
			this.cardPanel14 = new the_color_game_winform_version.CardPanel();
			this.cardPanel15 = new the_color_game_winform_version.CardPanel();
			this.cardPanel16 = new the_color_game_winform_version.CardPanel();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(12, 167);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(83, 14);
			this.label1.TabIndex = 2;
			this.label1.Text = "Coups restants :";
			// 
			// txt_remaining
			// 
			this.txt_remaining.Enabled = false;
			this.txt_remaining.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_remaining.Location = new System.Drawing.Point(101, 164);
			this.txt_remaining.Name = "txt_remaining";
			this.txt_remaining.Size = new System.Drawing.Size(57, 23);
			this.txt_remaining.TabIndex = 3;
			this.txt_remaining.Text = "4";
			this.txt_remaining.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// cardPanel1
			// 
			this.cardPanel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel1.IsRevealed = false;
			this.cardPanel1.Location = new System.Drawing.Point(12, 12);
			this.cardPanel1.Name = "cardPanel1";
			this.cardPanel1.Size = new System.Drawing.Size(32, 32);
			this.cardPanel1.TabIndex = 4;
			this.cardPanel1.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel2
			// 
			this.cardPanel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel2.IsRevealed = false;
			this.cardPanel2.Location = new System.Drawing.Point(50, 12);
			this.cardPanel2.Name = "cardPanel2";
			this.cardPanel2.Size = new System.Drawing.Size(32, 32);
			this.cardPanel2.TabIndex = 5;
			this.cardPanel2.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel3
			// 
			this.cardPanel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel3.IsRevealed = false;
			this.cardPanel3.Location = new System.Drawing.Point(88, 12);
			this.cardPanel3.Name = "cardPanel3";
			this.cardPanel3.Size = new System.Drawing.Size(32, 32);
			this.cardPanel3.TabIndex = 6;
			this.cardPanel3.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel4
			// 
			this.cardPanel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel4.IsRevealed = false;
			this.cardPanel4.Location = new System.Drawing.Point(123, 12);
			this.cardPanel4.Name = "cardPanel4";
			this.cardPanel4.Size = new System.Drawing.Size(32, 32);
			this.cardPanel4.TabIndex = 7;
			this.cardPanel4.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel5
			// 
			this.cardPanel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel5.IsRevealed = false;
			this.cardPanel5.Location = new System.Drawing.Point(12, 50);
			this.cardPanel5.Name = "cardPanel5";
			this.cardPanel5.Size = new System.Drawing.Size(32, 32);
			this.cardPanel5.TabIndex = 8;
			this.cardPanel5.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel6
			// 
			this.cardPanel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel6.IsRevealed = false;
			this.cardPanel6.Location = new System.Drawing.Point(50, 50);
			this.cardPanel6.Name = "cardPanel6";
			this.cardPanel6.Size = new System.Drawing.Size(32, 32);
			this.cardPanel6.TabIndex = 9;
			this.cardPanel6.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel7
			// 
			this.cardPanel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel7.IsRevealed = false;
			this.cardPanel7.Location = new System.Drawing.Point(88, 50);
			this.cardPanel7.Name = "cardPanel7";
			this.cardPanel7.Size = new System.Drawing.Size(32, 32);
			this.cardPanel7.TabIndex = 10;
			this.cardPanel7.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel8
			// 
			this.cardPanel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel8.IsRevealed = false;
			this.cardPanel8.Location = new System.Drawing.Point(123, 50);
			this.cardPanel8.Name = "cardPanel8";
			this.cardPanel8.Size = new System.Drawing.Size(32, 32);
			this.cardPanel8.TabIndex = 11;
			this.cardPanel8.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel9
			// 
			this.cardPanel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel9.IsRevealed = false;
			this.cardPanel9.Location = new System.Drawing.Point(12, 88);
			this.cardPanel9.Name = "cardPanel9";
			this.cardPanel9.Size = new System.Drawing.Size(32, 32);
			this.cardPanel9.TabIndex = 12;
			this.cardPanel9.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel10
			// 
			this.cardPanel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel10.IsRevealed = false;
			this.cardPanel10.Location = new System.Drawing.Point(50, 88);
			this.cardPanel10.Name = "cardPanel10";
			this.cardPanel10.Size = new System.Drawing.Size(32, 32);
			this.cardPanel10.TabIndex = 13;
			this.cardPanel10.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel11
			// 
			this.cardPanel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel11.IsRevealed = false;
			this.cardPanel11.Location = new System.Drawing.Point(88, 88);
			this.cardPanel11.Name = "cardPanel11";
			this.cardPanel11.Size = new System.Drawing.Size(32, 32);
			this.cardPanel11.TabIndex = 14;
			this.cardPanel11.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel12
			// 
			this.cardPanel12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel12.IsRevealed = false;
			this.cardPanel12.Location = new System.Drawing.Point(123, 88);
			this.cardPanel12.Name = "cardPanel12";
			this.cardPanel12.Size = new System.Drawing.Size(32, 32);
			this.cardPanel12.TabIndex = 15;
			this.cardPanel12.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel13
			// 
			this.cardPanel13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel13.IsRevealed = false;
			this.cardPanel13.Location = new System.Drawing.Point(12, 126);
			this.cardPanel13.Name = "cardPanel13";
			this.cardPanel13.Size = new System.Drawing.Size(32, 32);
			this.cardPanel13.TabIndex = 16;
			this.cardPanel13.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel14
			// 
			this.cardPanel14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel14.IsRevealed = false;
			this.cardPanel14.Location = new System.Drawing.Point(50, 126);
			this.cardPanel14.Name = "cardPanel14";
			this.cardPanel14.Size = new System.Drawing.Size(32, 32);
			this.cardPanel14.TabIndex = 17;
			this.cardPanel14.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel15
			// 
			this.cardPanel15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel15.IsRevealed = false;
			this.cardPanel15.Location = new System.Drawing.Point(85, 126);
			this.cardPanel15.Name = "cardPanel15";
			this.cardPanel15.Size = new System.Drawing.Size(32, 32);
			this.cardPanel15.TabIndex = 18;
			this.cardPanel15.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// cardPanel16
			// 
			this.cardPanel16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.cardPanel16.IsRevealed = false;
			this.cardPanel16.Location = new System.Drawing.Point(123, 126);
			this.cardPanel16.Name = "cardPanel16";
			this.cardPanel16.Size = new System.Drawing.Size(32, 32);
			this.cardPanel16.TabIndex = 19;
			this.cardPanel16.Click += new System.EventHandler(this.CardPanelClick);
			// 
			// frm_memory
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(167, 197);
			this.Controls.Add(this.cardPanel16);
			this.Controls.Add(this.cardPanel15);
			this.Controls.Add(this.cardPanel14);
			this.Controls.Add(this.cardPanel13);
			this.Controls.Add(this.cardPanel12);
			this.Controls.Add(this.cardPanel11);
			this.Controls.Add(this.cardPanel10);
			this.Controls.Add(this.cardPanel9);
			this.Controls.Add(this.cardPanel8);
			this.Controls.Add(this.cardPanel7);
			this.Controls.Add(this.cardPanel6);
			this.Controls.Add(this.cardPanel5);
			this.Controls.Add(this.cardPanel4);
			this.Controls.Add(this.cardPanel3);
			this.Controls.Add(this.cardPanel2);
			this.Controls.Add(this.cardPanel1);
			this.Controls.Add(this.txt_remaining);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frm_memory";
			this.Text = "ExtreMemory";
			this.Load += new System.EventHandler(this.Frm_memoryLoad);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_memoryFormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private the_color_game_winform_version.CardPanel cardPanel16;
		private the_color_game_winform_version.CardPanel cardPanel15;
		private the_color_game_winform_version.CardPanel cardPanel14;
		private the_color_game_winform_version.CardPanel cardPanel13;
		private the_color_game_winform_version.CardPanel cardPanel12;
		private the_color_game_winform_version.CardPanel cardPanel11;
		private the_color_game_winform_version.CardPanel cardPanel10;
		private the_color_game_winform_version.CardPanel cardPanel9;
		private the_color_game_winform_version.CardPanel cardPanel8;
		private the_color_game_winform_version.CardPanel cardPanel7;
		private the_color_game_winform_version.CardPanel cardPanel6;
		private the_color_game_winform_version.CardPanel cardPanel5;
		private the_color_game_winform_version.CardPanel cardPanel4;
		private the_color_game_winform_version.CardPanel cardPanel3;
		private the_color_game_winform_version.CardPanel cardPanel2;
		private the_color_game_winform_version.CardPanel cardPanel1;
		private System.Windows.Forms.TextBox txt_remaining;
		private System.Windows.Forms.Label label1;
	}
}
