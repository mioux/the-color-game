﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: SRUMEU
 * Date: 15/04/2015
 * Heure: 15:58
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
namespace the_color_game_winform_version
{
	partial class frm_color_match
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.color1 = new System.Windows.Forms.Label();
			this.color2 = new System.Windows.Forms.Label();
			this.color3 = new System.Windows.Forms.Label();
			this.color4 = new System.Windows.Forms.Label();
			this.color5 = new System.Windows.Forms.Label();
			this.color6 = new System.Windows.Forms.Label();
			this.color7 = new System.Windows.Forms.Label();
			this.color8 = new System.Windows.Forms.Label();
			this.tmr_decompte = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// color1
			// 
			this.color1.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.color1.Location = new System.Drawing.Point(12, 9);
			this.color1.Name = "color1";
			this.color1.Size = new System.Drawing.Size(350, 33);
			this.color1.TabIndex = 0;
			this.color1.Text = "--item1--";
			this.color1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.color1.Click += new System.EventHandler(this.ColorClick);
			// 
			// color2
			// 
			this.color2.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.color2.Location = new System.Drawing.Point(12, 42);
			this.color2.Name = "color2";
			this.color2.Size = new System.Drawing.Size(350, 33);
			this.color2.TabIndex = 1;
			this.color2.Text = "--item2--";
			this.color2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.color2.Click += new System.EventHandler(this.ColorClick);
			// 
			// color3
			// 
			this.color3.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.color3.Location = new System.Drawing.Point(12, 75);
			this.color3.Name = "color3";
			this.color3.Size = new System.Drawing.Size(350, 33);
			this.color3.TabIndex = 2;
			this.color3.Text = "--item3--";
			this.color3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.color3.Click += new System.EventHandler(this.ColorClick);
			// 
			// color4
			// 
			this.color4.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.color4.Location = new System.Drawing.Point(12, 108);
			this.color4.Name = "color4";
			this.color4.Size = new System.Drawing.Size(350, 33);
			this.color4.TabIndex = 3;
			this.color4.Text = "--item4--";
			this.color4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.color4.Click += new System.EventHandler(this.ColorClick);
			// 
			// color5
			// 
			this.color5.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.color5.Location = new System.Drawing.Point(12, 141);
			this.color5.Name = "color5";
			this.color5.Size = new System.Drawing.Size(350, 33);
			this.color5.TabIndex = 4;
			this.color5.Text = "--item5--";
			this.color5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.color5.Click += new System.EventHandler(this.ColorClick);
			// 
			// color6
			// 
			this.color6.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.color6.Location = new System.Drawing.Point(12, 174);
			this.color6.Name = "color6";
			this.color6.Size = new System.Drawing.Size(350, 33);
			this.color6.TabIndex = 5;
			this.color6.Text = "--item6--";
			this.color6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.color6.Click += new System.EventHandler(this.ColorClick);
			// 
			// color7
			// 
			this.color7.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.color7.Location = new System.Drawing.Point(12, 207);
			this.color7.Name = "color7";
			this.color7.Size = new System.Drawing.Size(350, 33);
			this.color7.TabIndex = 6;
			this.color7.Text = "--item7--";
			this.color7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.color7.Click += new System.EventHandler(this.ColorClick);
			// 
			// color8
			// 
			this.color8.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.color8.Location = new System.Drawing.Point(12, 240);
			this.color8.Name = "color8";
			this.color8.Size = new System.Drawing.Size(350, 33);
			this.color8.TabIndex = 7;
			this.color8.Text = "--item8--";
			this.color8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.color8.Click += new System.EventHandler(this.ColorClick);
			// 
			// tmr_decompte
			// 
			this.tmr_decompte.Interval = 90000;
			this.tmr_decompte.Tick += new System.EventHandler(this.tmr_decompte_Tick);
			// 
			// frm_color_match
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(374, 277);
			this.Controls.Add(this.color8);
			this.Controls.Add(this.color7);
			this.Controls.Add(this.color6);
			this.Controls.Add(this.color5);
			this.Controls.Add(this.color4);
			this.Controls.Add(this.color3);
			this.Controls.Add(this.color2);
			this.Controls.Add(this.color1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frm_color_match";
			this.Text = "ExtreMatch";
			this.Load += new System.EventHandler(this.Frm_color_matchLoad);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_color_matchFormClosing);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Timer tmr_decompte;
		private System.Windows.Forms.Label color4;
		private System.Windows.Forms.Label color8;
		private System.Windows.Forms.Label color7;
		private System.Windows.Forms.Label color6;
		private System.Windows.Forms.Label color5;
		private System.Windows.Forms.Label color3;
		private System.Windows.Forms.Label color2;
		private System.Windows.Forms.Label color1;
	}
}
