﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: SRUMEU
 * Date: 16/04/2015
 * Heure: 10:11
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

using the_color_game_memory;

namespace the_color_game_winform_version
{
	/// <summary>
	/// Description of frm_memory.
	/// </summary>
	public partial class frm_memory : Form
	{
		/// <summary>
		/// Form appelante.
		/// </summary>
		public frm_main CallerForm { get; set; }
		
		private CardPanel FirstRevealed;
		private CardPanel SecondRevealed;
		
		Memory _running;
		
		byte RemainingTry = 4;
		
		/// <summary>
		/// Constructeur.
		/// </summary>
		public frm_memory()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
            //

            this.Icon = Icon.FromHandle(Resources.icon.GetHicon());
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		
		void Frm_memoryFormClosing(object sender, FormClosingEventArgs e)
		{
			CallerForm.Show();
			this.Hide();
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		
		void CardPanelClick(object sender, EventArgs e)
		{
            // Uniquement le clic gauche.
            if (((MouseEventArgs)e).Button != System.Windows.Forms.MouseButtons.Left)
                return;

			CardPanel pan = (CardPanel)sender;
			
			// Ne fait rien si la carte a déjà été couplée ou si la carte n'entre pas en jeu.
			if (pan.Found == true)// || pan.IsSet == false)
				return;
			
			// J'ai cliqué sur une carte, il y en a déjà une affichée, il n'y en a pas 2, je n'ai pas recliqué sur la même.
			if (FirstRevealed != null && FirstRevealed.IsRevealed == true && (SecondRevealed == null || SecondRevealed.IsRevealed == false) && FirstRevealed.UniqueId != pan.UniqueId)
			{
				SecondRevealed = pan;

                if (SecondRevealed.HiddenColor.ColorValue == FirstRevealed.HiddenColor.ColorValue)
                {
                    FirstRevealed.SetFound();
                    SecondRevealed.SetFound();
                    FirstRevealed = null;
                    SecondRevealed = null;
                }
                else
                {
                    --RemainingTry;
                }

				txt_remaining.Text = RemainingTry.ToString();

                pan.Reveal();
                CheckWin();
				
				if (RemainingTry == 0)
				{
					MessageBox.Show("Perdu !");
					this.Close();
				}
			}
			else
			{
				if (FirstRevealed != null && FirstRevealed.Found == false)
					FirstRevealed.BackHidden();
				if (SecondRevealed != null && SecondRevealed.Found == false)
					SecondRevealed.BackHidden();
				
				FirstRevealed = pan;
				pan.Reveal();
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		
		private void CheckWin()
		{
			bool allFound = true;
			
			foreach (Control ctrl in this.Controls)
			{
				if (ctrl is CardPanel)
				{
					CardPanel pan = (CardPanel)ctrl;
					
					allFound = allFound && (pan.Found == true || pan.IsSet == false);
				}
			}
			
			if (allFound == true)
			{
				MessageBox.Show("Gagné !");
				this.Close();
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		
		void Frm_memoryLoad(object sender, EventArgs e)
		{
			_running = new Memory(CallerForm.NbColor, CallerForm.IsHard);
			
			for (byte i = 1; i <= CallerForm.NbColor * 2; ++i)
			{
				Control[] search = Controls.Find("cardPanel" + i, true);
				if (search.Length == 1)
				{
					CardPanel pan = (CardPanel)search[0];
					pan.SetColorRef(_running.GetColorRef(Convert.ToByte(i - 1)));
				}
			}
		}
	}
}
