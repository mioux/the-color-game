﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: SRUMEU
 * Date: 15/04/2015
 * Heure: 15:58
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using the_color_game_color_match;

namespace the_color_game_winform_version
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class frm_color_match : Form
	{
		/// <summary>
		/// Form appelante.
		/// </summary>
		public frm_main CallerForm { get; set; }
		
        /// <summary>
        /// Un jeu est en cours.
        /// </summary>
		private bool _isRunning = false;
		
        /// <summary>
        /// Score courant.
        /// </summary>
		private short _score = 0;
		
        /// <summary>
        /// Objet du jeu.
        /// </summary>
		private ColorMatch _running;
		
        /// <summary>
        /// Constructeur.
        /// </summary>
		public frm_color_match()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//

            this.Icon = Icon.FromHandle(Resources.icon.GetHicon());
		}
		
        /// <summary>
        /// Réinitialisation des textes à vide.
        /// </summary>
        void Reinit()
        {
            color1.Text = "";
            color2.Text = "";
            color3.Text = "";
            color4.Text = "";
            color5.Text = "";
            color6.Text = "";
            color7.Text = "";
            color8.Text = "";
        }
		
        /// <summary>
        /// Clic sur un label de couleur.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void ColorClick(object sender, EventArgs e)
		{
			Label lbl = (Label)sender;
			
			if (_isRunning == true && Convert.ToByte(lbl.Name.Substring(lbl.Name.Length - 1)) == _running.Winner + 1)
			{
				++_score;
			}
			
			NewGame();
		}
		
        /// <summary>
        /// Démarrage d'un nouveau jeu.
        /// </summary>
		private void NewGame()
        {
            _running = new ColorMatch(8, CallerForm.NbColor, CallerForm.IsHard);

            color1.Text = _running.GetName(0);
            color2.Text = _running.GetName(1);
            color3.Text = _running.GetName(2);
            color4.Text = _running.GetName(3);
            color5.Text = _running.GetName(4);
            color6.Text = _running.GetName(5);
            color7.Text = _running.GetName(6);
            color8.Text = _running.GetName(7);

            color1.ForeColor = _running.GetColor(0);
            color2.ForeColor = _running.GetColor(1);
            color3.ForeColor = _running.GetColor(2);
            color4.ForeColor = _running.GetColor(3);
            color5.ForeColor = _running.GetColor(4);
            color6.ForeColor = _running.GetColor(5);
            color7.ForeColor = _running.GetColor(6);
            color8.ForeColor = _running.GetColor(7);
		}

        /// <summary>
        /// Démarrage d'un jeu en fonction de la difficulté sélectionnée.s
        /// </summary>
        void Start()
        {
            _score = 0;
            NewGame();
            _isRunning = true;
            tmr_decompte.Start();
        }

        /// <summary>
        /// Fin du jeu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmr_decompte_Tick(object sender, EventArgs e)
        {
            tmr_decompte.Stop();
            MessageBox.Show(string.Format("Score: {0}", _score));
            _isRunning = false;
            
            Reinit();
            this.Close();
        }
		
		void Frm_color_matchFormClosing(object sender, FormClosingEventArgs e)
		{
			CallerForm.Show();
			this.Hide();
            tmr_decompte.Stop();
		}
		
		void Frm_color_matchShown(object sender, EventArgs e)
		{
            Reinit();
            Start();
		}
		
		void Frm_color_matchLoad(object sender, EventArgs e)
		{
			Reinit();
			Start();
		}
	}
}
