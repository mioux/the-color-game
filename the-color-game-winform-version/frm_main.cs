﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: SRUMEU
 * Date: 16/04/2015
 * Heure: 10:08
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace the_color_game_winform_version
{
	/// <summary>
	/// Description of frm_main.
	/// </summary>
	public partial class frm_main : Form
	{
		frm_color_match match;
		frm_memory memory;
		
		public bool IsHard { get { return rbn_cyborg.Checked; } private set {}}
		public byte NbColor { get { return Convert.ToByte(trk_nbColor.Value); } private set {}}
		
		public frm_main()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//

            this.Icon = Icon.FromHandle(Resources.icon.GetHicon());
		}
		
		void Btn_colorMatchClick(object sender, EventArgs e)
		{
			match = new frm_color_match();
			match.CallerForm = this;
			match.Show();
			this.Hide();
		}
		
		void Btn_memoryClick(object sender, EventArgs e)
		{
			memory = new frm_memory();
			memory.CallerForm = this;
			memory.Show();
			this.Hide();
		}
	}
}
