﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: SRUMEU
 * Date: 16/04/2015
 * Heure: 10:08
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
namespace the_color_game_winform_version
{
	partial class frm_main
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.btn_colorMatch = new System.Windows.Forms.Button();
			this.btn_memory = new System.Windows.Forms.Button();
			this.trk_nbColor = new System.Windows.Forms.TrackBar();
			this.label1 = new System.Windows.Forms.Label();
			this.rbn_facile = new System.Windows.Forms.RadioButton();
			this.rbn_cyborg = new System.Windows.Forms.RadioButton();
			((System.ComponentModel.ISupportInitialize)(this.trk_nbColor)).BeginInit();
			this.SuspendLayout();
			// 
			// btn_colorMatch
			// 
			this.btn_colorMatch.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_colorMatch.Location = new System.Drawing.Point(12, 138);
			this.btn_colorMatch.Name = "btn_colorMatch";
			this.btn_colorMatch.Size = new System.Drawing.Size(174, 23);
			this.btn_colorMatch.TabIndex = 0;
			this.btn_colorMatch.Text = "Color Match";
			this.btn_colorMatch.UseVisualStyleBackColor = true;
			this.btn_colorMatch.Click += new System.EventHandler(this.Btn_colorMatchClick);
			// 
			// btn_memory
			// 
			this.btn_memory.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_memory.Location = new System.Drawing.Point(12, 167);
			this.btn_memory.Name = "btn_memory";
			this.btn_memory.Size = new System.Drawing.Size(174, 23);
			this.btn_memory.TabIndex = 1;
			this.btn_memory.Text = "Memory";
			this.btn_memory.UseVisualStyleBackColor = true;
			this.btn_memory.Click += new System.EventHandler(this.Btn_memoryClick);
			// 
			// trk_nbColor
			// 
			this.trk_nbColor.Location = new System.Drawing.Point(16, 35);
			this.trk_nbColor.Maximum = 8;
			this.trk_nbColor.Minimum = 4;
			this.trk_nbColor.Name = "trk_nbColor";
			this.trk_nbColor.Size = new System.Drawing.Size(170, 45);
			this.trk_nbColor.TabIndex = 2;
			this.trk_nbColor.Value = 8;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(16, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(170, 17);
			this.label1.TabIndex = 3;
			this.label1.Text = "Nombre de couleurs";
			// 
			// rbn_facile
			// 
			this.rbn_facile.Checked = true;
			this.rbn_facile.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rbn_facile.Location = new System.Drawing.Point(16, 78);
			this.rbn_facile.Name = "rbn_facile";
			this.rbn_facile.Size = new System.Drawing.Size(170, 24);
			this.rbn_facile.TabIndex = 4;
			this.rbn_facile.TabStop = true;
			this.rbn_facile.Text = "Facile";
			this.rbn_facile.UseVisualStyleBackColor = true;
			// 
			// rbn_cyborg
			// 
			this.rbn_cyborg.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rbn_cyborg.Location = new System.Drawing.Point(16, 108);
			this.rbn_cyborg.Name = "rbn_cyborg";
			this.rbn_cyborg.Size = new System.Drawing.Size(170, 24);
			this.rbn_cyborg.TabIndex = 5;
			this.rbn_cyborg.TabStop = true;
			this.rbn_cyborg.Text = "Cyborg";
			this.rbn_cyborg.UseVisualStyleBackColor = true;
			// 
			// frm_main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(198, 202);
			this.Controls.Add(this.rbn_cyborg);
			this.Controls.Add(this.rbn_facile);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.trk_nbColor);
			this.Controls.Add(this.btn_memory);
			this.Controls.Add(this.btn_colorMatch);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frm_main";
			this.Text = "The color game";
			((System.ComponentModel.ISupportInitialize)(this.trk_nbColor)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.TrackBar trk_nbColor;
		private System.Windows.Forms.RadioButton rbn_cyborg;
		private System.Windows.Forms.RadioButton rbn_facile;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btn_memory;
		private System.Windows.Forms.Button btn_colorMatch;
	}
}
