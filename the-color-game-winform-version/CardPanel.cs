﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: SRUMEU
 * Date: 16/04/2015
 * Heure: 16:03
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using the_color_game_color_ref;

namespace the_color_game_winform_version
{
	/// <summary>
	/// Description of CardPanel.
	/// </summary>
	public partial class CardPanel : UserControl
	{
		public bool IsRevealed { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
		public ColorRef HiddenColor { get; private set; }
		
		/// <summary>
		/// 
		/// </summary>
		public bool IsSet { get; private set; }
		
		/// <summary>
		/// 
		/// </summary>
		public bool Found { get; private set; }
		
		/// <summary>
		/// 
		/// </summary>
		public string UniqueId { get; private set; }
		
		/// <summary>
		/// 
		/// </summary>
		public CardPanel()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			
			IsSet = false;
			IsRevealed = false;
			Found = false;
			UniqueId = Guid.NewGuid().ToString();
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="col"></param>
		public void SetColorRef(ColorRef col)
		{
			if (IsSet == true)
			{
				throw new InvalidOperationException("Ne peut affecter 2x la couleur de la carte.");
			}
			
			IsSet = true;
			
			this.BackColor = Color.FromArgb(128, 64, 0);
			
			HiddenColor = col;
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void Reveal()
		{
			if (IsSet == true)
			{
				this.BackColor = HiddenColor.ColorValue;
				IsRevealed = true;
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void BackHidden()
		{
            if (Found == false)
            {
                this.BackColor = Color.FromArgb(128, 64, 0);
            }
            IsRevealed = false;
		}
		
		/// <summary>
		/// 
		/// </summary>
		public void SetFound()
		{
			Found = true;
            IsRevealed = false;
		}
	}
}
