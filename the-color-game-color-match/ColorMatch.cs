﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: SRUMEU
 * Date: 15/04/2015
 * Heure: 16:34
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using System.Collections.Generic;
using the_color_game_color_ref;
using System.Drawing;

namespace the_color_game_color_match
{
	/// <summary>
	/// Description of MyClass.
	/// </summary>
	public class ColorMatch
	{
		/// <summary>
		/// Liste des couleurs en jeu.
		/// </summary>
		private List<ColorRef> ColorList = new List<ColorRef>();
		
		/// <summary>
		/// Liste des noms en jeu.
		/// </summary>
		private List<ColorRef> ColorNameList = new List<ColorRef>();
		
		/// <summary>
		/// Item gagnant.
		/// </summary>
		public byte Winner { get; private set; }
		
		/// <summary>
		/// Nombre de couleurs générées.
		/// </summary>
		private byte NbMatch { get; set; }
		
		/// <summary>
		/// Nouveau jeu.
		/// </summary>
		/// <param name="nbMatch">Nombre de couleurs à générer.</param>
		/// <param name="levelMax">Nombre max de couleurs à prendre en compte.</param>
		public ColorMatch(byte nbMatch, byte levelMax, bool isHard)
		{
			NbMatch = nbMatch;
			
			if (levelMax < 4)
			{
				levelMax = 4;
			}
			if (levelMax > 7)
			{
				levelMax = 7;
			}
			
			Random r = new Random();
			
			List<Byte> colorsList = new List<byte>();
			List<Byte> namesList = new List<byte>();
			
			for (byte i = 0; i < nbMatch; ++i)
			{
				colorsList.Add(Convert.ToByte(r.Next(0, levelMax)));
			}
			
			for (byte i = 0; i < nbMatch; ++i)
			{
				namesList.Add(Convert.ToByte(r.Next(0, levelMax)));
			}
			
			for (byte i = 0; i < nbMatch; ++i)
			{
				while (colorsList[i] == namesList[i])
				{
					namesList[i] = Convert.ToByte(r.Next(0, levelMax));
				}
			}
			
			Winner = Convert.ToByte(r.Next(0, nbMatch));
			
			namesList[Winner] = colorsList[Winner];
			
			for (byte i = 0; i < nbMatch; ++i)
			{
				ColorList.Add(ColorRef.FromIndex(colorsList[i], isHard));
				ColorNameList.Add(ColorRef.FromIndex(namesList[i], isHard));
			}
		}
		
		/// <summary>
		/// Renvoie la couleur liée à l'index.
		/// </summary>
		/// <param name="idx"></param>
		/// <returns></returns>
		
		public Color GetColor(byte idx)
		{
			if (idx > NbMatch)
			{
				OutOfRange();
			}
			
			return ColorList[idx].ColorValue;
		}
		
		/// <summary>
		/// Renvoie le nom liée à l'index.
		/// </summary>
		/// <param name="idx"></param>
		/// <returns></returns>
		
		public string GetName(byte idx)
		{
			if (idx > NbMatch)
			{
				OutOfRange();
			}
			
			return ColorNameList[idx].ColorName;
		}
		
		/// <summary>
		/// Erreur hors limite.
		/// </summary>
		
		private void OutOfRange()
		{
			throw new IndexOutOfRangeException(string.Format("Il n'y a que {0} couleurs générées", NbMatch));
		}
	}
}