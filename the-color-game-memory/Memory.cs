﻿/*
 * Crée par SharpDevelop.
 * Utilisateur: SRUMEU
 * Date: 16/04/2015
 * Heure: 10:55
 * 
 * Pour changer ce modèle utiliser Outils | Options | Codage | Editer les en-têtes standards.
 */
using System;
using System.Collections.Generic;
using the_color_game_color_ref;

namespace the_color_game_memory
{
	/// <summary>
	/// Je de memory.
	/// </summary>
	public class Memory
	{
		/// <summary>
		/// 
		/// </summary>
		private byte NbColors { get; set; }
		
		/// <summary>
		/// 
		/// </summary>
		private List<ColorRef> ColorList = new List<ColorRef>();
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="nbColors"></param>
		/// <param name="isHard"></param>
		public Memory(byte nbColors, bool isHard)
		{
			NbColors = nbColors;
			
			List<ColorRef> orderedList = new List<ColorRef>();
			
			for (byte i = 0; i < 2*nbColors; ++i)
			{
				orderedList.Add(ColorRef.FromIndex(Convert.ToByte(i / 2), isHard));
			}
			
			Random r = new Random();
			
			while (orderedList.Count > 0)
			{
				int idx = r.Next(0, orderedList.Count - 1);
				ColorList.Add(orderedList[idx]);
				orderedList.RemoveAt(idx);
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="idx"></param>
		public ColorRef GetColorRef(byte idx)
		{
			if (idx > NbColors * 2)
				throw new IndexOutOfRangeException(string.Format("Seuls {0} éléments sont disponibles", NbColors * 2));
			
			return ColorList[idx];
		}
	}
}